import 'package:flutter/material.dart';

Widget iconBeforeData({
  required Widget icon,
  required String data,
}) {
  return Container(
    height: 45,
    child: Row(
      children: [
        icon,
        Expanded(
          child: Center(
            child: Text(
              data,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
        ),
      ],
    ),
  );
}
