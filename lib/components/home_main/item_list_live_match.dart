import 'package:base_flutter_framework/components/home_main/icon_before_data.dart';
import 'package:base_flutter_framework/components/widget/circle_widget_bordered.dart';
import 'package:base_flutter_framework/core/models/live_match_model.dart';
import 'package:flutter/material.dart';

Widget itemListLiveMatch({required LiveMatchModel liveMatch}) {
  return Container(
    height: 200,
    child: Column(
      children: [
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 10),
              child: circleWidgetBordered(
                  child:
                      liveMatch.avatar ?? Container(child: Icon(Icons.person)),
                  maxRadius: 22.5,
                  widthBorder: 2,
                  colorBorder: Colors.blue,
                  background: Colors.red),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    liveMatch.name,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  Row(
                    children: [
                      CircleAvatar(
                        child: Container(child: Icon(Icons.person)),
                        backgroundColor: Colors.blue,
                        maxRadius: 14,
                      ),
                      Text(
                        liveMatch.scoreHome.toString(),
                        style: TextStyle(fontSize: 15),
                      ),
                      Text(
                        "vs",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.orange, fontSize: 10),
                      ),
                      Text(
                        liveMatch.scoreAway.toString(),
                        style: TextStyle(fontSize: 15),
                      ),
                      CircleAvatar(
                        child: Container(child: Icon(Icons.person)),
                        backgroundColor: Colors.red,
                        maxRadius: 14,
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
        iconBeforeData(
            icon: Image.asset(
              "assets/icons/coolicon.png",
              // height: 30,
              // width: 30,
            ),
            data: liveMatch.location),
        iconBeforeData(
            icon: Image.asset(
              "assets/icons/Vector.png",
              // height: 30,
              // width: 30,
            ),
            data: liveMatch.time),
        iconBeforeData(
            icon: Image.asset(
              "assets/icons/Union.png",
              // height: 30,
              // width: 30,
            ),
            data: liveMatch.money.toString()),
      ],
    ),
  );
}
