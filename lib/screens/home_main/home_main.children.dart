part of 'home_main_screen.dart';

extension _HomeScreenChildern on HomeMainScreen {
  Widget _messageWidget({required int countMessage}) {
    return TextButton(
        style: ButtonAppStyle().buttonWrap(),
        onPressed: () {},
        child: Container());
  }

  Widget buttonInviatation(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2.5,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.blue[800],
            ),
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 10),
                  child: Icon(
                    Icons.email_outlined,
                    color: Colors.white,
                  ),
                ),
                Text(
                  "Inviatation",
                  style: TextStyle(
                      color: Colors.white,
                      height: 0.9,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
          Align(
            heightFactor: 15,
            widthFactor: 15,
            alignment: Alignment.topRight,
            child: circleWidgetBordered(
              maxRadius: 7.5,
              widthBorder: 1,
              colorBorder: Colors.white,
              background: Colors.red,
              child: Container(
                child: Text(
                  "9",
                  style: TextStyle(fontSize: 10),
                ),
              ),
            ),
            // child: CircleAvatar(
            //   maxRadius: 9,
            //   backgroundColor: Colors.white,
            //   child: Container(
            //     padding: EdgeInsets.all(2),
            //     child: CircleAvatar(
            //       child: Container(
            //         child: Text(
            //           "9",
            //           style: TextStyle(fontSize: 12),
            //         ),
            //       ),
            //       backgroundColor: Colors.red,
            //     ),
            //   ),
            // ),
          )
        ],
      ),
    );
  }

  Widget avatar() {
    return Container(
      width: 50,
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 3),
            child: circleWidgetBordered(
                child: Container(child: Icon(Icons.person)),
                maxRadius: 22.5,
                widthBorder: 2,
                colorBorder: Colors.blue,
                background: Colors.red),
          ),
          Align(
            heightFactor: 16,
            widthFactor: 16,
            alignment: Alignment.bottomRight,
            child: circleWidgetBordered(
              maxRadius: 8,
              widthBorder: 2,
              colorBorder: Colors.grey,
              background: Colors.white,
              child: Image.asset("assets/icons/edit.png")
            ),
          )
        ],
      ),
    );
  }

  Widget header(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 30),
            height: 65,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [buttonInviatation(context), avatar()],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
            height: 65,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  "assets/icons/coolicon.png",
                  // height: 30,
                  // width: 30,
                ),
                Text(
                  "247 Johnston Ferry",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                CircleAvatar(
                  child: Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                  maxRadius: 17.5,
                  backgroundColor: Colors.orange[900],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget tabBar() {
    return Container(
      height: 400,
      width: 400,
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: TabBar(
            tabs: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                //color: Colors.grey,
                child: Row(
                  children: [
                    Icon(Icons.circle),
                    Text(
                      "Live Match",
                    ),
                  ],
                ),
              ),
              Container(
                //color: Colors.grey,
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  children: [
                    Icon(Icons.circle),
                    Text(
                      "History Match",
                    ),
                  ],
                ),
              ),
            ],
            indicatorPadding: EdgeInsets.symmetric(horizontal: 30),
            indicatorWeight: 3,
            indicatorColor: Colors.orange[900],
            labelColor: Colors.orange[900],
            labelStyle: TextStyle(fontWeight: FontWeight.bold),
            labelPadding: EdgeInsets.only(left: 40),
            unselectedLabelColor: Colors.grey,
            unselectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
          ),
          body: TabBarView(
            children: [
              itemListLiveMatch(
                  liveMatch: LiveMatchModel(
                      name: "Dat",
                      scoreHome: 7,
                      scoreAway: 7,
                      location: "Ho chi Minh",
                      time: "gfjgshsbgvyjbfvubvubub",
                      money: 2000)),
              Text(
                "History Match",
                style: TextStyle(color: Colors.grey),
              )
            ],
          ),
        ),
      ),
    );
  }
}
