import 'package:base_flutter_framework/components/home_main/item_list_live_match.dart';
import 'package:base_flutter_framework/components/widget/circle_widget_bordered.dart';
import 'package:base_flutter_framework/components/widget/scaffold_padding.dart';
import 'package:base_flutter_framework/core/models/live_match_model.dart';
import 'package:base_flutter_framework/utils/style/button_style.dart';
import 'package:base_flutter_framework/utils/style/text_style.dart';
import 'package:flutter/material.dart';
part 'home_main.children.dart';

class HomeMainScreen extends StatefulWidget {
  const HomeMainScreen({Key? key}) : super(key: key);

  @override
  _HomeMainScreenState createState() => _HomeMainScreenState();
}

class _HomeMainScreenState extends State<HomeMainScreen> {
  @override
  Widget build(BuildContext context) {
    return scaffoldPadding(
      paddingBool: true,
        backgroundColor: Colors.white,
        context: context,
        child: Container(
          child: Column(
            children: [
              widget.header(context),
              widget.tabBar()
            ],
          ),
        ));
  }
}
