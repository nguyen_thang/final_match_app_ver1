import 'package:base_flutter_framework/utils/hex_color.dart';
import 'package:flutter/material.dart';

class ColorCommon {
  static Color colorGrey = HexColor("cccccc");
  static Color colorGrey2 = HexColor("333333");
  static Color colorGreen = HexColor("78a807");
  static Color colorGreen2 = HexColor("46b13f");

  static Color colorHintText = HexColor("757575");
  static Color colorBgIconMenu = HexColor("f4f4f4");
  static Color colorIcon = HexColor("a7a7a7");

  static Color liveAutionGreen = HexColor("1ea8a4");
  static Color backkgroundTimeLine = HexColor("eeeeee");

  //Color new app
  static final Color colorButtonGoogle = HexColor("DE4A39");
  static final Color colorButtonFacebook = HexColor("4367B2");
  static final Color colorButtonApple = HexColor("000000");
}
